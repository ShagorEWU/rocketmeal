import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:flutter/foundation.dart' as Foundation;
import 'package:rocketMeal/Presentation/Cart/cart_screen.dart';
import 'Common/DependencyInjection/dependencyInjection.dart';
import 'Router/routeName.dart';
import 'Router/routes.dart';

void main() {
  // SystemChrome.setSystemUIOverlayStyle(
  //     SystemUiOverlayStyle(statusBarColor: Colors.white));
  // SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
  //   statusBarIconBrightness: Brightness.dark,
  //   statusBarBrightness: Brightness.light,
  //   systemNavigationBarColor: Colors.blue, // navigation bar color
  //   statusBarColor: Colors.grey, // status bar color
  // ));

  // SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
  //     // statusBarColor is used to set Status bar color in Android devices.
  //     statusBarColor: Colors.transparent,

  //     // To make Status bar icons color white in Android devices.
  //     statusBarIconBrightness: Brightness.dark,

  //     // statusBarBrightness is used to set Status bar icon color in iOS.
  //     statusBarBrightness: Brightness.dark
  //     // Here dark means light color Status bar icons.

  //     ));

  runApp(ScreenUtilInit(
    designSize: Size(360, 690),
    allowFontScaling: false,
    child: AnnotatedRegion<SystemUiOverlayStyle>(
      value: Platform.isAndroid
          ? SystemUiOverlayStyle.light.copyWith(
              // To make Status bar icons color white in Android devices.
              statusBarIconBrightness: Brightness.dark,
              statusBarColor: Colors.white,
              systemNavigationBarColor: Colors.black12,
              systemNavigationBarIconBrightness: Brightness.dark,
            )
          : SystemUiOverlayStyle.dark.copyWith(
              // statusBarBrightness is used to set Status bar icon color in iOS.
              statusBarBrightness: Brightness.light,
              statusBarColor: Colors.white,
              systemNavigationBarColor: Colors.black12,
              systemNavigationBarIconBrightness: Brightness.light,
            ),
      child: GetMaterialApp(
        defaultTransition: Transition.fadeIn,
        initialBinding: DependencyInjection(),
        transitionDuration: Duration(milliseconds: 200),
        enableLog: Foundation.kDebugMode ? true : false,
        initialRoute: RouteName.onBoardScreen,
        getPages: Routes.route,
        theme: ThemeData(primarySwatch: Colors.blue, fontFamily: 'ubuntu'),
        debugShowCheckedModeBanner: false,
      ),
    ),
  ));
}
