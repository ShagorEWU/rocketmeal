// To parse this JSON data, do
//
//     final loginModel = loginModelFromJson(jsonString);

import 'dart:convert';

LoginModel loginModelFromJson(String str) =>
    LoginModel.fromJson(json.decode(str));

String loginModelToJson(LoginModel data) => json.encode(data.toJson());

class LoginModel {
  LoginModel({
    this.message,
    this.errorCode,
    this.data,
  });

  String message;
  String errorCode;
  List<Datum> data;

  factory LoginModel.fromJson(Map<String, dynamic> json) => LoginModel(
        message: json["message"] == null ? null : json["message"],
        errorCode: json["errorCode"] == null ? null : json["errorCode"],
        data: json["data"] == null
            ? null
            : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "message": message == null ? null : message,
        "errorCode": errorCode == null ? null : errorCode,
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.adminId,
    this.adminName,
    this.email,
    this.phoneNumber,
    this.adminPassword,
    this.activeStatus,
    this.createdOn,
    this.updatedOn,
  });

  String adminId;
  String adminName;
  String email;
  String phoneNumber;
  String adminPassword;
  String activeStatus;
  DateTime createdOn;
  DateTime updatedOn;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        adminId: json["admin_id"] == null ? null : json["admin_id"],
        adminName: json["admin_name"] == null ? null : json["admin_name"],
        email: json["email"] == null ? null : json["email"],
        phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
        adminPassword:
            json["admin_password"] == null ? null : json["admin_password"],
        activeStatus:
            json["active_status"] == null ? null : json["active_status"],
        createdOn: json["created_on"] == null
            ? null
            : DateTime.parse(json["created_on"]),
        updatedOn: json["updated_on"] == null
            ? null
            : DateTime.parse(json["updated_on"]),
      );

  Map<String, dynamic> toJson() => {
        "admin_id": adminId == null ? null : adminId,
        "admin_name": adminName == null ? null : adminName,
        "email": email == null ? null : email,
        "phone_number": phoneNumber == null ? null : phoneNumber,
        "admin_password": adminPassword == null ? null : adminPassword,
        "active_status": activeStatus == null ? null : activeStatus,
        "created_on": createdOn == null ? null : createdOn.toIso8601String(),
        "updated_on": updatedOn == null ? null : updatedOn.toIso8601String(),
      };
}
