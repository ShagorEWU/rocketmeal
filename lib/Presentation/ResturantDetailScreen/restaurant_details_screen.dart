import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:get/get.dart';
import 'package:rocketMeal/Common/Widgets/dark_overlay.dart';
import 'package:rocketMeal/Common/Widgets/heading_row.dart';
import 'package:rocketMeal/Common/Widgets/potbelly_button.dart';
import 'package:rocketMeal/Common/Widgets/spaces.dart';
import 'package:rocketMeal/Presentation/Home/Components/card_tags.dart';
import 'package:rocketMeal/values/data.dart';
import 'package:rocketMeal/values/values.dart';

class ResturantDetailsScreen extends StatefulWidget {
  ResturantDetailsScreen({Key key}) : super(key: key);

  @override
  _ResturantDetailsScreenState createState() => _ResturantDetailsScreenState();
}

class _ResturantDetailsScreenState extends State<ResturantDetailsScreen>
    with SingleTickerProviderStateMixin {
  ScrollController _scrollViewController;
  TabController _controller;
  int index = 0;
  Icon iconLove = Icon(
    FlutterIcons.favorite_border_mdi,
    color: Colors.red,
  );
  // List<dynamic> responseList = FOOD_DATA;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // iconLove = Icon(FlutterIcons.favorite_border_mdi);
    // Create TabController for getting the index of current tab
    _controller = TabController(length: 4, vsync: this);
    _scrollViewController = ScrollController(initialScrollOffset: 0.0);
    _controller.addListener(() {
      print("Selected Index: " + _controller.index.toString());
    });
  }

  TextStyle addressTextStyle = Styles.customNormalTextStyle(
    color: AppColors.accentText,
    fontSize: Sizes.TEXT_SIZE_14,
  );

  TextStyle openingTimeTextStyle = Styles.customNormalTextStyle(
    color: Colors.red,
    fontSize: Sizes.TEXT_SIZE_14,
  );

  TextStyle subHeadingTextStyle = Styles.customTitleTextStyle(
    color: AppColors.headingText,
    fontWeight: FontWeight.w600,
    fontSize: Sizes.TEXT_SIZE_18,
  );

  BoxDecoration fullDecorations = Decorations.customHalfCurvedButtonDecoration(
    color: Colors.black.withOpacity(0.1),
    topleftRadius: 24,
    bottomleftRadius: 24,
    topRightRadius: 24,
    bottomRightRadius: 24,
  );
  BoxDecoration leftSideDecorations =
      Decorations.customHalfCurvedButtonDecoration(
    color: Colors.black.withOpacity(0.1),
    topleftRadius: 24,
    bottomleftRadius: 24,
  );

  BoxDecoration rightSideDecorations =
      Decorations.customHalfCurvedButtonDecoration(
    color: Colors.black.withOpacity(0.1),
    topRightRadius: 24,
    bottomRightRadius: 24,
  );

  @override
  Widget build(BuildContext context) {
    List<String> args = Get.arguments;
//    final RestaurantDetails args = ModalRoute.of(context).settings.arguments;
    var heightOfStack = MediaQuery.of(context).size.height / 2.8;
    var aPieceOfTheHeightOfStack = heightOfStack - heightOfStack / 3.5;
    return Scaffold(
      body: SafeArea(
          top: true,
          left: false,
          right: false,
          child: DefaultTabController(
            length: 4,
            child: NestedScrollView(
                controller: _scrollViewController,
                headerSliverBuilder:
                    (BuildContext context, bool innerBoxIsScrolled) {
                  return <Widget>[
                    SliverAppBar(
                      // toolbarHeight: ,
                      expandedHeight: 400.0,
                      backgroundColor: Colors.white,
                      pinned: true,
                      floating: false,
                      toolbarHeight: 48,
                      // snap: true,

                      // forceElevated: boxIsScrolled,
                      title: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            args[6].toString(),
                            style: TextStyle(color: Colors.black, fontSize: 20),
                          )),
                      leading: InkWell(
                        onTap: () => {Get.back()}, //AppRouter.navigator.pop(),
                        child: Padding(
                            padding: const EdgeInsets.only(
                              left: Sizes.MARGIN_16,
                              right: Sizes.MARGIN_16,
                            ),
                            child: Icon(
                              Icons.arrow_back_ios,
                              color: Colors.black,
                            )),
                      ),
                      actions: [
                        InkWell(
                            child: iconLove,
                            onTap: () {
                              iconLove ==
                                      Icon(
                                        FlutterIcons.favorite_border_mdi,
                                        color: Colors.red,
                                      )
                                  ? iconLove = Icon(
                                      FlutterIcons.favorite_mdi,
                                      color: Colors.red,
                                    )
                                  : iconLove = Icon(
                                      FlutterIcons.favorite_border_mdi,
                                      color: Colors.red,
                                    );
                            })
                      ],

                      // here the desired height

                      flexibleSpace: FlexibleSpaceBar(
                        background: Image.asset(
                          args[1].toString(),
                          fit: BoxFit.fitWidth,
                          height: 400,
                        ),
                      ),

                      bottom: PreferredSize(
                        preferredSize: Size.fromHeight(50),
                        child: Container(
                          child: Column(
                            children: [
                              Container(
                                padding:
                                    const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                height: 50,
                                child: Row(
                                  children: <Widget>[
                                    Text(
                                      args[0].toString(),
                                      textAlign: TextAlign.left,
                                      style: Styles.customTitleTextStyle(
                                        color: AppColors.headingText,
                                        fontWeight: FontWeight.w600,
                                        fontSize: Sizes.TEXT_SIZE_20,
                                      ),
                                    ),
                                    SizedBox(width: 4.0),
                                    CardTags(
                                      title: args[2],
                                      decoration: BoxDecoration(
                                        gradient: Gradients.secondaryGradient,
                                        boxShadow: [
                                          Shadows.secondaryShadow,
                                        ],
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(8.0)),
                                      ),
                                    ),
                                    SizedBox(width: 4.0),
                                    CardTags(
                                      title: args[5],
                                      decoration: BoxDecoration(
                                        color:
                                            Color.fromARGB(255, 132, 141, 255),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(8.0)),
                                      ),
                                    ),
                                    Spacer(flex: 1),
                                    Container(
                                      child: Row(
                                        children: [
                                          Image.asset(
                                            ImagePath.starIcon,
                                            height: Sizes.WIDTH_14,
                                            width: Sizes.WIDTH_14,
                                          ),
                                          Text(
                                            args[4].toString(),
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              TabBar(
                                labelStyle: TextStyle(fontSize: 15.0),
                                unselectedLabelStyle: TextStyle(fontSize: 12.0),
                                indicator: BoxDecoration(color: Colors.green[50]
                                    // color: _hasBeenPressed ? Color(0xffffffff) : Color(0xffff00a8),
                                    ),
                                unselectedLabelColor: Colors.green[50],
                                indicatorColor: Colors.green[50],
                                tabs: [
                                  Tab(
                                      child: Text(
                                    " All ",
                                    style: TextStyle(color: Colors.black),
                                  )),
                                  Tab(
                                      child: Text(
                                    " BreakFast ",
                                    style: TextStyle(color: Colors.black),
                                  )),
                                  Tab(
                                    child: Text(
                                      "Launch",
                                      style: TextStyle(color: Colors.black),
                                    ),
                                  ),
                                  Tab(
                                      child: Text(
                                    "Dinner",
                                    style: TextStyle(color: Colors.black),
                                  )),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                      // SliverPadding(
                      //   padding: new EdgeInsets.all(16.0),
                      //   sliver: new SliverList(
                      //     delegate: new SliverChildListDelegate([

                      //         ],
                      //       ),
                      //     ]),
                      //   ),
                    ),
                  ];
                },
                body: TabBarView(children: <Widget>[
                  ListView.builder(
                    shrinkWrap: true,
                    itemCount: 12,
                    itemBuilder: (context, index) {
                      return Container(
                        height: 70,
                        child: Card(
                          elevation: 1,
                          shadowColor: Colors.white,
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(10, 15, 10, 10),
                            child: Row(
                              children: [
                                Expanded(
                                  child: Container(
                                    child: Column(
                                      children: [
                                        Align(
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                              foodNameAll[index],
                                            )),
                                        Container(
                                          width:
                                              MediaQuery.of(context).size.width,
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.baseline,
                                            textBaseline:
                                                TextBaseline.ideographic,
                                            children: [
                                              Text(
                                                "Tk ${presentPrice[index]}  ",
                                                style: TextStyle(
                                                    fontSize: 14,
                                                    color: Colors.black),
                                              ),
                                              Text(
                                                "Tk ${presentPrice[index]}",
                                                style: TextStyle(
                                                    fontSize: 12.5,
                                                    color: Colors.red,
                                                    decoration: TextDecoration
                                                        .lineThrough),
                                              )
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                                Container(
                                  alignment: Alignment.topRight,
                                  child: Row(
                                    // crossAxisAlignment: CrossAxisAlignment.end,
                                    // mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      Visibility(
                                        visible: _itemCount[index] != 0
                                            ? true
                                            : false,
                                        child: new IconButton(
                                          icon: new Icon(Icons.remove),
                                          onPressed: () => setState(
                                              () => _itemCount[index]--),
                                        ),
                                      ),
                                      Visibility(
                                          visible: _itemCount[index] != 0
                                              ? true
                                              : false,
                                          child:
                                              new Text("${_itemCount[index]}")),
                                      new IconButton(
                                          icon: new Icon(Icons.add),
                                          onPressed: () => setState(
                                              () => _itemCount[index]++))
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                  ListView.builder(
                    shrinkWrap: true,
                    itemCount: 12,
                    itemBuilder: (context, index) {
                      return Container(
                        height: 70,
                        child: Card(
                          elevation: 1,
                          shadowColor: Colors.white,
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(10, 15, 10, 10),
                            child: Row(
                              children: [
                                Expanded(
                                  child: Container(
                                    child: Column(
                                      children: [
                                        Align(
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                              foodNameAll[index],
                                            )),
                                        Container(
                                          width:
                                              MediaQuery.of(context).size.width,
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.baseline,
                                            textBaseline:
                                                TextBaseline.ideographic,
                                            children: [
                                              Text(
                                                "Tk ${presentPrice[index]}  ",
                                                style: TextStyle(
                                                    fontSize: 14,
                                                    color: Colors.black),
                                              ),
                                              Text(
                                                "Tk ${presentPrice[index]}",
                                                style: TextStyle(
                                                    fontSize: 12.5,
                                                    color: Colors.red,
                                                    decoration: TextDecoration
                                                        .lineThrough),
                                              )
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                                Container(
                                  alignment: Alignment.topRight,
                                  child: Row(
                                    // crossAxisAlignment: CrossAxisAlignment.end,
                                    // mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      Visibility(
                                        visible: _itemCount[index] != 0
                                            ? true
                                            : false,
                                        child: new IconButton(
                                          icon: new Icon(Icons.remove),
                                          onPressed: () => setState(
                                              () => _itemCount[index]--),
                                        ),
                                      ),
                                      Visibility(
                                          visible: _itemCount[index] != 0
                                              ? true
                                              : false,
                                          child:
                                              new Text("${_itemCount[index]}")),
                                      new IconButton(
                                          icon: new Icon(Icons.add),
                                          onPressed: () => setState(
                                              () => _itemCount[index]++))
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                  ListView.builder(
                    shrinkWrap: true,
                    itemCount: 12,
                    itemBuilder: (context, index) {
                      return Container(
                        height: 70,
                        child: Card(
                          elevation: 1,
                          shadowColor: Colors.white,
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(10, 15, 10, 10),
                            child: Row(
                              children: [
                                Expanded(
                                  child: Container(
                                    child: Column(
                                      children: [
                                        Align(
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                              foodNameAll[index],
                                            )),
                                        Container(
                                          width:
                                              MediaQuery.of(context).size.width,
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.baseline,
                                            textBaseline:
                                                TextBaseline.ideographic,
                                            children: [
                                              Text(
                                                "Tk ${presentPrice[index]}  ",
                                                style: TextStyle(
                                                    fontSize: 14,
                                                    color: Colors.black),
                                              ),
                                              Text(
                                                "Tk ${presentPrice[index]}",
                                                style: TextStyle(
                                                    fontSize: 12.5,
                                                    color: Colors.red,
                                                    decoration: TextDecoration
                                                        .lineThrough),
                                              )
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                                Container(
                                  alignment: Alignment.topRight,
                                  child: Row(
                                    // crossAxisAlignment: CrossAxisAlignment.end,
                                    // mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      Visibility(
                                        visible: _itemCount[index] != 0
                                            ? true
                                            : false,
                                        child: new IconButton(
                                          icon: new Icon(Icons.remove),
                                          onPressed: () => setState(
                                              () => _itemCount[index]--),
                                        ),
                                      ),
                                      Visibility(
                                          visible: _itemCount[index] != 0
                                              ? true
                                              : false,
                                          child:
                                              new Text("${_itemCount[index]}")),
                                      new IconButton(
                                          icon: new Icon(Icons.add),
                                          onPressed: () => setState(
                                              () => _itemCount[index]++))
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                  ListView.builder(
                    shrinkWrap: true,
                    itemCount: 12,
                    itemBuilder: (context, index) {
                      return Container(
                        height: 70,
                        child: Card(
                          elevation: 1,
                          shadowColor: Colors.white,
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(10, 15, 10, 10),
                            child: Row(
                              children: [
                                Expanded(
                                  child: Container(
                                    child: Column(
                                      children: [
                                        Align(
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                              foodNameAll[index],
                                            )),
                                        Container(
                                          width:
                                              MediaQuery.of(context).size.width,
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.baseline,
                                            textBaseline:
                                                TextBaseline.ideographic,
                                            children: [
                                              Text(
                                                "Tk ${presentPrice[index]}  ",
                                                style: TextStyle(
                                                    fontSize: 14,
                                                    color: Colors.black),
                                              ),
                                              Text(
                                                "Tk ${presentPrice[index]}",
                                                style: TextStyle(
                                                    fontSize: 12.5,
                                                    color: Colors.red,
                                                    decoration: TextDecoration
                                                        .lineThrough),
                                              )
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                                Container(
                                  alignment: Alignment.topRight,
                                  child: Row(
                                    // crossAxisAlignment: CrossAxisAlignment.end,
                                    // mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      Visibility(
                                        visible: _itemCount[index] != 0
                                            ? true
                                            : false,
                                        child: new IconButton(
                                          icon: new Icon(Icons.remove),
                                          onPressed: () => setState(
                                              () => _itemCount[index]--),
                                        ),
                                      ),
                                      Visibility(
                                          visible: _itemCount[index] != 0
                                              ? true
                                              : false,
                                          child:
                                              new Text("${_itemCount[index]}")),
                                      new IconButton(
                                          icon: new Icon(Icons.add),
                                          onPressed: () => setState(
                                              () => _itemCount[index]++))
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ])),
          )),
    );
  }

  List<String> foodNameAll = [
    "Kacci Biriyani",
    "Morog Polao",
    "Burger",
    "Pizza",
    "Dal",
    "Vat",
    "Khicuri",
    "Kacci Biriyani",
    "Morog Polao",
    "Burger",
    "Pizza",
    "Dal",
    "Vat",
    "Khicuri",
  ];
  List<String> presentPrice = [
    "100",
    "120",
    "130",
    "100",
    "120",
    "130",
    "100",
    "120",
    "130",
    "100",
    "120",
    "130",
    "100",
    "120",
  ];
  List<String> pastPrice = [
    "110",
    "130",
    "150",
    "110",
    "130",
    "150",
    "110",
    "130",
    "150",
    "110",
    "130",
    "150",
    "110",
    "130",
  ];
  List<int> _itemCount = [
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
  ];

  List<Widget> createUserListTiles({@required numberOfUsers}) {
    List<Widget> userListTiles = [];
    List<String> imagePaths = [
      ImagePath.profile1,
      ImagePath.profile4,
      ImagePath.profile3,
      ImagePath.profile4,
      ImagePath.profile1,
    ];
    List<String> userNames = [
      "Collin Fields",
      "Sherita Burns",
      "Bill Sacks",
      "Romeo Folie",
      "Pauline Cobbina",
    ];

    List<String> description = [
      "Lorem Ipsum baga fada",
      "Lorem Ipsum baga fada",
      "Lorem Ipsum baga fada",
      "Lorem Ipsum baga fada",
      "Lorem Ipsum baga fada",
    ];
    List<String> ratings = [
      "4.0",
      "3.0",
      "5.0",
      "2.0",
      "4.0",
    ];

    List<int> list = List<int>.generate(numberOfUsers, (i) => i + 1);

    list.forEach((i) {
      userListTiles.add(ListTile(
        leading: Image.asset(imagePaths[i - 1]),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              userNames[i - 1],
              style: subHeadingTextStyle,
            ),
            //Ratings(ratings[i - 1]),
          ],
        ),
        contentPadding: EdgeInsets.symmetric(horizontal: 0),
        subtitle: Text(
          description[i - 1],
          style: addressTextStyle,
        ),
      ));
    });
    return userListTiles;
  }
}
