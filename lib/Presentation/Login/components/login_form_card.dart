import 'package:rocketMeal/Common/Variables/TextList.dart';
import 'package:rocketMeal/Common/Widgets/Button/radioButton.dart';
import 'package:rocketMeal/Common/Widgets/Others/InputText.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class FormCard extends StatefulWidget {
  const FormCard({
    Key key,
    // ignore: non_constant_identifier_names
    @required this.UserName,
    // ignore: non_constant_identifier_names

    // ignore: non_constant_identifier_names
    @required this.Password,
    // ignore: non_constant_identifier_names
  }) : super(key: key);
  // ignore: non_constant_identifier_names
  final TextEditingController UserName;
  // ignore: non_constant_identifier_names
  final TextEditingController Password;

  @override
  _FormCardState createState() => _FormCardState();
}

class _FormCardState extends State<FormCard> {
  bool _isSelected = false;

  void _radio() {
    setState(() {
      _isSelected = !_isSelected;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      width: double.infinity,
//      height: ScreenUtil.getInstance().setHeight(500),
      padding: EdgeInsets.only(bottom: 1),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8.0),
      ),
      child: Padding(
        padding: EdgeInsets.only(left: 0.0, right: 0.0, top: 16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 00.0),
              child: Image.asset("asset/login.jpg"),
            ),
            SizedBox(
              height: 30,
            ),
            InputText(
              txt: "username",
              textController: widget.UserName,
            ),
            SizedBox(
              height: ScreenUtil().setHeight(25),
            ),
            InputText(txt: "password", textController: widget.Password),
            SizedBox(
              height: ScreenUtil().setHeight(25),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  width: 12.0,
                ),
                GestureDetector(
                  onTap: _radio,
                  child: radioButton(_isSelected),
                ),
                SizedBox(
                  width: 10.0,
                ),
                CustomText.textRemember,
              ],
            )
          ],
        ),
      ),
    );
  }
}
