import 'dart:collection';

import 'package:rocketMeal/Common/Methods/CommonMethod.dart';
import 'package:rocketMeal/Common/Variables/TextList.dart';
import 'package:rocketMeal/Router/routeName.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import 'Block/login_block.dart';
import 'components/login_form_card.dart';

// ignore: camel_case_types
class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => new _LoginScreenState();
}

// ignore: camel_case_types
class _LoginScreenState extends State<LoginScreen> {
  bool _isSelected = false;
  final LoginBloc _loginBloc = Get.put(LoginBloc());
  CommonMethod commonMethod = Get.find();

  TextEditingController loginUserName = new TextEditingController();
  TextEditingController loginPassWord = new TextEditingController();
  void _radio() {
    setState(() {
      _isSelected = !_isSelected;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // loginUserName.text.isEmpty = "";
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomPadding: true,
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.only(left: 28.0, right: 28.0, top: 28.0),
              child: Column(
                children: <Widget>[
                  FormCard(UserName: loginUserName, Password: loginPassWord),
                  Padding(padding: const EdgeInsets.only(top: 20.0)),
                  //Login Form Card
                  Center(
                    child: SizedBox(
                      height: 55,
                      child: Center(
                        child: Row(
                          //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            InkWell(
                              child: Container(
                                // width: ScreenUtil().setWidth(300),
                                // height: ScreenUtil().setHeight(100),
                                // padding: EdgeInsets.only(left: 44.0),
                                width: MediaQuery.of(context).size.width - 56.0,

                                decoration: BoxDecoration(
                                  color: Colors.green,
                                  borderRadius: BorderRadius.circular(45.0),
                                ),
                                child: Material(
                                  color: Colors.transparent,
                                  child: InkWell(
                                    onTap: () {
                                      if (loginUserName.text.isEmpty) {
                                      } else {
                                        //print(_loginBloc.emailTextController.text);
                                        Map<String, dynamic> map =
                                            new HashMap();
                                        map["phoneNumber"] =
                                            loginUserName.text.toString();
                                        map["password"] = loginPassWord.text;

                                        _loginBloc.submitLoginData(map);
                                      }
                                      //Navigator.pushNamed(context, "/otp");
                                    },
                                    child: Center(
                                      child: CustomText.textSignIn,
                                    ),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),

                  SizedBox(
                    height: 20,
                  ),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      TextButton(
                        onPressed: () {
                          Navigator.pushReplacementNamed(
                              context, "/resetPassword",
                              arguments: {});
                        },
                        child: CustomText.textForgetPassword,
                      ),
                      Padding(padding: const EdgeInsets.only(left: 20.0)),
                      CustomText.newUser,
                      InkWell(
                        onTap: () {
                          Get.toNamed(RouteName.registrationScreen);
                        },
                        child: CustomText.textRegistrationn,
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      //Icon(Icons.)
                      Icon(FlutterIcons.facebook_f_faw),
                      Icon(FlutterIcons.google_ant),
                    ],
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
