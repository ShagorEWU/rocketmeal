import 'package:rocketMeal/Controller/Active.dart';
import 'package:rocketMeal/Controller/DarkMode.dart';
import 'package:rocketMeal/Controller/Notifications.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'dart:core';
import 'package:lite_rolling_switch/lite_rolling_switch.dart';
import 'package:get/get.dart';

// ignore: camel_case_types
class settings_informations extends StatefulWidget {
  settings_informations({Key key, @required this.txt}) : super(key: key);
  final String txt;
  @override
  _settings_informationsState createState() => _settings_informationsState();
}

// ignore: camel_case_types
class _settings_informationsState extends State<settings_informations> {
  final notificationscontroller = Get.put(NotificationsController());
  final darkmodecontroller = Get.put(DarkModeController());
  final activecontroller = Get.put(ActiveController());
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.lightGreen[100],
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Row(
          children: [
            Expanded(
              child: Text(
                widget.txt,
                style: TextStyle(fontSize: 18, color: Colors.black),
              ),
            ),
            LiteRollingSwitch(
              //initial value
              value: widget.txt == "Notifications"
                  ? notificationscontroller.notifications.value.status.value
                  : widget.txt == "ActiveStatus"
                      ? activecontroller.active.value.status.value
                      : darkmodecontroller.darkMode.value.status.value,

              colorOn: Colors.greenAccent[700],
              colorOff: Colors.redAccent[700],
              iconOn: Icons.done,
              iconOff: Icons.remove_circle_outline,
              textSize: 16.0,
              onChanged: (bool state) {
                //Use it to manage the different states

                widget.txt == "Notifications"
                    ? notificationscontroller.notifications.value.status.value =
                        state
                    : widget.txt == "ActiveStatus"
                        ? activecontroller.active.value.status.value = state
                        : darkmodecontroller.darkMode.value.status.value =
                            state;
              },
            )
          ],
        ),
      ),
    );
  }
}
