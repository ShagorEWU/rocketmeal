import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rocketMeal/Common/Widgets/BottomNavBar/bottomNavBar.dart';

import 'components/body.dart';

class ProfileScreen extends StatelessWidget {
  //static String routeName = "/profile";
  static int index = 4;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Body(),
      bottomNavigationBar: BottomNavBar(
        activeBar: ProfileScreen.index,
      ),
    );
  }
}
