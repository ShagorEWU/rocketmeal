import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:rocketMeal/values/values.dart';

import 'search_input_field.dart';

class AppBarHome extends StatefulWidget {
  AppBarHome({Key key}) : super(key: key);

  @override
  _AppBarState createState() => _AppBarState();
}

class _AppBarState extends State<AppBarHome> {
  TextEditingController controller = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 140,
      child: Column(
        children: [
          Row(
            children: [
              Icon(
                FlutterIcons.location_evi,
                size: 40,
              ),
              Expanded(
                  child: Text(
                "417 Doom St, California, CA 90013, USA",
                textAlign: TextAlign.left,
                overflow: TextOverflow.ellipsis,
              )),
              IconButton(
                  icon: Icon(
                    FlutterIcons.ios_notifications_outline_ion,
                    size: 40,
                  ),
                  onPressed: () {})
            ],
          ),
          FoodyBiteSearchInputField(
            ImagePath.searchIcon,
            controller: controller,
            textFormFieldStyle:
                Styles.customNormalTextStyle(color: AppColors.accentText),
            hintText: StringConst.HINT_TEXT_HOME_SEARCH_BAR,
            hintTextStyle:
                Styles.customNormalTextStyle(color: AppColors.accentText),
            suffixIconImagePath: ImagePath.settingsIcon,
            borderWidth: 0.0,
            // onTapOfLeadingIcon: () => AppRouter.navigator.pushNamed(
            //   AppRouter.searchResultsScreen,
            //   arguments: SearchValue(
            //     controller.text,
            //   ),
            // ),
            // onTapOfSuffixIcon: () =>
            //     AppRouter.navigator.pushNamed(AppRouter.filterScreen)
            borderStyle: BorderStyle.solid,
          ),
          SizedBox(height: 16.0),
        ],
      ),
    );
  }
}
