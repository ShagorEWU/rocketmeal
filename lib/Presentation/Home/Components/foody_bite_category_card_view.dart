import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:rocketMeal/values/data.dart';

import 'category_card.dart';

class FoodyBiteCategoryCardView extends StatefulWidget {
  FoodyBiteCategoryCardView({Key key}) : super(key: key);

  @override
  _FoodyBiteCategoryCardViewState createState() =>
      _FoodyBiteCategoryCardViewState();
}

class _FoodyBiteCategoryCardViewState extends State<FoodyBiteCategoryCardView> {
  @override
  Widget build(BuildContext context) {
    return Container(
      //transform:  Matrix4.translationValues(0.0, -80.0, 0.0),
      height: 80,
      child: ListView.builder(
        physics: ScrollPhysics(),
        //  shrinkWrap: true,
        // physics: NeverScrollableScrollPhysics(),
        scrollDirection: Axis.horizontal,
        itemCount: categoryImagePaths.length,
        itemBuilder: (context, index) {
          return Container(
            margin: EdgeInsets.only(right: 8.0),
            child: FoodyBiteCategoryCard(
              imagePath: categoryImagePaths[index],
              gradient: gradients[index],
              category: category[index],
            ),
          );
        },
      ),
    );
  }
}
