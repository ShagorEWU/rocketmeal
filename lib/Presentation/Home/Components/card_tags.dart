import 'package:rocketMeal/values/values.dart';
import 'package:flutter/material.dart';

class CardTags extends StatelessWidget {
  final String title;
  final BoxDecoration decoration;

  CardTags({
    @required this.title,
    this.decoration,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Opacity(
        opacity: 0.7,
        child: Container(
          width: 50,
          height: 20,
          decoration: decoration,
          child: Center(
            child: Text(
              title,
              textAlign: TextAlign.center,
              style: Styles.customNormalTextStyle(
                fontSize: Sizes.TEXT_SIZE_8,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
