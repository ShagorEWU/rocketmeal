import 'package:flutter/services.dart';
import 'package:rocketMeal/values/data.dart';
import 'package:rocketMeal/values/values.dart';
import 'package:flutter/material.dart';

import 'appbar.dart';
import 'category_card.dart';
import 'food_card_view.dart';
import 'foody_bite_card.dart';
import 'foody_bite_category_card_view.dart';
import 'heading_row.dart';
import 'search_input_field.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    // SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    //   statusBarIconBrightness: Brightness.dark,
    //   statusBarBrightness: Brightness.dark,
    //   //  systemNavigationBarColor: Colors.blue, // navigation bar color
    //   statusBarColor: Colors.grey, // status bar color
    // ));

    return Column(
      children: [
        AppBarHome(),
        //SizedBox(height: 160.0),
        // SingleChildScrollView(
        //   //controller: controller,
        //   child: Column(
        //     children: [
        //       SizedBox(height: 160.0),
        //       SizedBox(height: 160.0),
        //       SizedBox(height: 160.0),
        //       SizedBox(height: 160.0),
        //       SizedBox(height: 160.0),
        //       SizedBox(height: 160.0),
        //       Container(
        //         child: Text("ddddddd"),
        //       )
        //     ],
        //   ),
        // ),
        Expanded(
          child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            physics: ScrollPhysics(),

            //physics: BouncingScrollPhysics(),
            child: Padding(
              // height: double.infinity,
              padding: const EdgeInsets.only(left: 10, right: 10),
              child: Column(
                children: [
                  HeadingRow(
                    title: StringConst.TRENDING_RESTAURANTS,
                    number: StringConst.SEE_ALL_45,
                    // onTapOfNumber: () => AppRouter.navigator
                    //     .pushNamed(AppRouter.trendingRestaurantsScreen),
                  ),
                  SizedBox(height: 16.0),
                  FoodCardView(),
                  // FoodCardView(),
                  // Transform(),
                  SizedBox(height: 32.0),
                  HeadingRow(
                    title: StringConst.CATEGORY,
                    number: StringConst.SEE_ALL_9,
                    // onTapOfNumber: () =>
                    // AppRouter.navigator.pushNamed(AppRouter.categoriesScreen),
                  ),
                  SizedBox(height: 16.0),
                  FoodyBiteCategoryCardView(),
                  SizedBox(height: 32.0),
                  HeadingRow(
                    title: StringConst.CATEGORY,
                    number: StringConst.SEE_ALL_9,
                    // onTapOfNumber: () =>
                    // AppRouter.navigator.pushNamed(AppRouter.categoriesScreen),
                  ),
                  SizedBox(height: 16.0),
                  FoodyBiteCategoryCardView(),
                  SizedBox(height: 16.0),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
