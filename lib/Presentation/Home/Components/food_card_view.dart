import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:rocketMeal/Router/routeName.dart';
import 'package:rocketMeal/values/data.dart';

import 'foody_bite_card.dart';

class FoodCardView extends StatefulWidget {
  FoodCardView({Key key}) : super(key: key);

  @override
  _FoodCardViewState createState() => _FoodCardViewState();
}

class _FoodCardViewState extends State<FoodCardView> {
  List<String> argument = new List(7);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 240,
      width: MediaQuery.of(context).size.width,
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: 3,
          itemBuilder: (context, index) {
            return Container(
              margin: EdgeInsets.only(right: 4.0),
              child: FoodyBiteCard(
                onTap: () {
                  setState(() {
                    argument[0] = restaurantNames[index];

                    argument[1] = imagePaths[index];
                    argument[2] = category[index];

                    argument[3] = addresses[index];
                    argument[4] = ratings[index];

                    argument[5] = distance[index];
                    argument[6] = status[index];
                  });
                  Get.toNamed(RouteName.resturantDetailsScreen,
                      arguments: argument);
                },
                imagePath: imagePathsNew[index],
                status: status[index],
                cardTitle: restaurantNames[index],
                rating: ratings[index],
                category: category[index],
                distance: distance[index],
                address: addresses[index],
              ),
            );
          }),
    );
  }
}
