import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'home_screen.dart';

class Body extends StatefulWidget {
  Body({Key key}) : super(key: key);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  @override
  Widget build(BuildContext context) {
    return HomeScreen();
  }
}
