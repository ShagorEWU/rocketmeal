import 'package:flutter/material.dart';

import 'package:rocketMeal/values/values.dart';

import 'food_card_view.dart';

import 'foody_bite_category_card_view.dart';
import 'heading_row.dart';

class NewWindow extends StatefulWidget {
  NewWindow({Key key}) : super(key: key);

  @override
  _NewWindowState createState() => _NewWindowState();
}

class _NewWindowState extends State<NewWindow> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        physics: ScrollPhysics(),
        // dragStartBehavior: DragStartBehavior.start,

        //physics: BouncingScrollPhysics(),
        child: Container(
          //height: double.infinity,
          padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
          child: SizedBox(
            child: Column(
              children: [
                HeadingRow(
                  title: StringConst.TRENDING_RESTAURANTS,
                  number: StringConst.SEE_ALL_45,
                  // onTapOfNumber: () => AppRouter.navigator
                  //     .pushNamed(AppRouter.trendingRestaurantsScreen),
                ),
                SizedBox(height: 16.0),
                FoodCardView(),
                // FoodCardView(),
                // Transform(),
                SizedBox(height: 32.0),
                HeadingRow(
                  title: StringConst.CATEGORY,
                  number: StringConst.SEE_ALL_9,
                  // onTapOfNumber: () =>
                  // AppRouter.navigator.pushNamed(AppRouter.categoriesScreen),
                ),
                SizedBox(height: 16.0),
                FoodyBiteCategoryCardView(),
                SizedBox(height: 32.0),
                HeadingRow(
                  title: StringConst.CATEGORY,
                  number: StringConst.SEE_ALL_9,
                  // onTapOfNumber: () =>
                  // AppRouter.navigator.pushNamed(AppRouter.categoriesScreen),
                ),
                SizedBox(height: 16.0),
                FoodyBiteCategoryCardView(),
                SizedBox(height: 16.0),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
