import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:rocketMeal/Common/Widgets/BottomNavBar/bottomNavBar.dart';

import 'Components/body.dart';

class HomeScreen extends StatefulWidget {
  static int index = 0;

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  bool showStatusBar = true;
  SystemUiOverlay systemforBothAndroidIos;
  SystemUiOverlay systemforIOS;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      bottom: false,
      right: false,
      left: false,
      child: new Scaffold(
        // resizeToAvoidBottomInset: false,
        // appBar: AppBar(),
        body: Container(child: Body()),
        bottomNavigationBar: BottomNavBar(
          activeBar: HomeScreen.index,
        ),
      ),
    );
  }
}
