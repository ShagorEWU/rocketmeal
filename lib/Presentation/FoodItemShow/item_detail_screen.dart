import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:rocketMeal/Presentation/FoodItemShow/Components/body.dart';

class ItemDetailScreen extends StatefulWidget {
  ItemDetailScreen({Key key}) : super(key: key);

  @override
  _ItemDetailScreenState createState() => _ItemDetailScreenState();
}

class _ItemDetailScreenState extends State<ItemDetailScreen> {
  @override
  Widget build(BuildContext context) {
    var argument = Get.arguments;
    // String args = Get.arguments;
    return Body(argmnt: argument);
  }
}
