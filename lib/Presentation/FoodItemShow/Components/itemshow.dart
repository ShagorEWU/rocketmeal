import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:get/get.dart';
import 'package:rocketMeal/Controller/item.dart';
import 'package:rocketMeal/Presentation/FoodItemShow/Bloc/item_show_bloc.dart';
import 'package:rocketMeal/Presentation/Home/Components/card_tags.dart';
import 'package:rocketMeal/Router/routeName.dart';
import 'package:rocketMeal/values/values.dart';
import 'package:rocketMeal/Controller/bottomshow.dart';
import 'package:rocketMeal/Common/Database/database_helper.dart';
import 'show_modal_bottom_sheet.dart';

class itemList {
  String name;
  int price;
  int amount;
}

class ItemShow extends StatefulWidget {
  final args;

  ItemShow({
    Key key,
    this.args,
  }) : super(key: key);
  @override
  _ItemShowState createState() => _ItemShowState();
}

class _ItemShowState extends State<ItemShow>
    with SingleTickerProviderStateMixin {
  int showSnack = 0;
  double ht;
  BottomShowController bottomtroller = Get.put(BottomShowController());
  ItemShowController itemcon = Get.put(ItemShowController());
  //ItemsController itemscontroller = Get.put(ItemsController());
  ScrollController _scrollViewController;
  TabController _controller;
  int index = 0;
  int bottomsheet = 0;
  Icon iconLove = Icon(
    FlutterIcons.favorite_border_mdi,
    color: Colors.red,
  );

  List<itemList> newList = [];
  List<Map<String, dynamic>> currentItem = [];
  void checkState(String name, int index) async {
    _itemCount[index] =
        await DatabaseHelper.instance.querySingleRow(foodNameAll[index]);
    // print(name);
    // List<Map<String, dynamic>> asdf =
    //     await DatabaseHelper.instance.querySingleRow(name);
    // print("DzzzzzzDDD");
    // print(asdf);
    // print("DzzzzztttttttttzDDD");
    // print(asdf.length);
    // if (asdf == []) {
    //   _itemCount[index] = 0;
    // } else {
    //   _itemCount[index] = asdf[0]["amount"];
    // }
    // print("ITEM");
    // print(_itemCount[index]);

    // ;
  }

  void getItemValue(String name, int index) {
    checkState(name, index);
  }

  anotherListState() {}

  Widget setItemList() {
    return FutureBuilder<List<Map<String, dynamic>>>(
        future: itemcon.generateCustomList(newList),
        builder: (BuildContext context,
            AsyncSnapshot<List<Map<String, dynamic>>> snapshot) {
          if (snapshot.hasData) {
            print("again get data or not  $snapshot.data.length");
            return setUI(snapshot.data);
          } else {
            return CircularProgressIndicator();
          }
        });
  }

  Future<List<Map<String, dynamic>>> generateCustomList(
      List<itemList> newListTemp) async {
    List<Map<String, dynamic>> customList = [];

    for (int i = 0; i < newListTemp.length; i++) {
      int amountTemp =
          await DatabaseHelper.instance.querySingleRow(newListTemp[i].name);

      if (amountTemp > 0) {
        customList.add({
          "name": newListTemp[i].name,
          "price": newListTemp[i].price,
          "amount": amountTemp
        });
      } else {
        customList.add({
          "name": newListTemp[i].name,
          "price": newListTemp[i].price,
          "amount": 0
        });
      }
    }

    return customList;
  }

  Widget setUI(List<Map<String, dynamic>> itemListt) {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: itemListt.length,
      itemBuilder: (context, index) {
        return Container(
            height: 100,
            child: Card(
                elevation: 1,
                shadowColor: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(10, 15, 10, 5),
                  child: Expanded(
                    child: Container(
                      child: Column(
                        children: [
                          Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                itemListt[index]["name"],
                              )),
                          Container(
                            width: MediaQuery.of(context).size.width,
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.baseline,
                              textBaseline: TextBaseline.ideographic,
                              children: [
                                TextButton(
                                  child: Text(
                                    "Tk ${itemListt[index]["price"]}  ",
                                    style: TextStyle(
                                        fontSize: 14, color: Colors.black),
                                  ),
                                  onPressed: () {
                                    showModalBottomSheet<void>(
                                      context: context,
                                      isScrollControlled: true,
                                      useRootNavigator: false,
                                      builder: (_) {
                                        return ShowModalBottomSheet(
                                            name: itemListt[index]["name"]);
                                      },
                                    );
                                  },
                                ),
                                Text(
                                  "Tk ${itemListt[index]["price"]}",
                                  style: TextStyle(
                                      fontSize: 12.5,
                                      color: Colors.red,
                                      decoration: TextDecoration.lineThrough),
                                ),
                                Container(
                                  alignment: Alignment.topRight,
                                  child: Row(
                                    children: [
                                      Visibility(
                                        //             Visibility(
                                        visible: itemListt[index]["amount"] != 0
                                            ? true
                                            : false,
                                        child: new IconButton(
                                          icon: new Icon(Icons.remove),
                                          onPressed: () => setState(() {
                                            print(newList);
                                            print("DttD");
                                            print(newList[index].name);
                                            print(newList.length);
                                            newList[index].amount--;
                                            print("DttD");
                                            // currentItem[i]['name'] = currentItem[i]['name'] + 1;
                                            print(newList[index].amount);

                                            //print(ssss);

                                            if (newList[index].amount == 0) {
                                              String txt = newList[index].name;
                                              newList.removeAt(index);
                                              DatabaseHelper.instance
                                                  .delete(txt);
                                            } else {
                                              // print(currentItem[i]['amount']);
                                              Map<String, dynamic> map = {
                                                "name": newList[index].name,
                                                "price": newList[index].price,
                                                "amount": newList[index].amount
                                              };
                                              DatabaseHelper.instance.update(
                                                map,
                                              );
                                              print(map['amount']);
                                            }
                                          }),
                                        ),
                                      ),
                                      Visibility(
                                          visible:
                                              itemListt[index]["amount"] != 0
                                                  ? true
                                                  : false,
                                          child: new Text(
                                              "${itemListt[index]["amount"]}")),
                                      new IconButton(
                                          icon: new Icon(Icons.add),
                                          onPressed: () => setState(() {
                                                print(newList);
                                                print("DttD");
                                                // int ssss = 1;

                                                // //currentItem[i]["amount"];
                                                // print("DttD");
                                                // ssss = ssss + 1;
                                                print(newList[1].name);
                                                print(newList.length);

                                                print("DttD");
                                                // currentItem[i]['name'] = currentItem[i]['name'] + 1;

                                                //print(ssss);
                                                itemListt[index]["amount"]++;
                                                // bottomtroller
                                                //         .currentItemList[index]
                                                // ["amount"]++;
                                                print(newList[index].amount);
                                                Map<String, dynamic> map = {
                                                  "name": itemListt[index]
                                                      ["name"],
                                                  "price": itemListt[index]
                                                      ["price"],
                                                  "amount": itemListt[index]
                                                      ["amount"]
                                                };
                                                if (itemListt[index]
                                                        ["amount"] ==
                                                    1) {
                                                  DatabaseHelper.instance
                                                      .insert(map);
                                                } else {
                                                  // print(currentItem[i]['amount']);

                                                  DatabaseHelper.instance
                                                      .update(
                                                    map,
                                                  );
                                                  print(map['amount']);
                                                }
                                              }))
                                    ],
                                  ),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                )));
      },
    );
  }

  // List<dynamic> responseList = FOOD_DATA;
  @override
  void initState() {
    //  List<Map<String, dynamic>> asdf =
    //     await DatabaseHelper.instance.querySingleRow(name);
    for (int i = 0; i < foodNameAll.length; i++) {
      getItemValue(foodNameAll[i], i);

      itemList uuu = new itemList();
      uuu.amount = _itemCount[i];

      uuu.price = presentPrice[i];
      uuu.name = foodNameAll[i];
      newList.add(uuu);

      //setItemList();
    }
    //print(" length = ${newList.length}");

    // TODO: implement initState
    super.initState();
    //ht = icontroller.bottomShow.value.height.value;
    // iconLove = Icon(FlutterIcons.favorite_border_mdi);
    // Create TabController for getting the index of current tab
    _controller = TabController(length: 1, vsync: this);
    _scrollViewController = ScrollController(initialScrollOffset: 0.0);
    _controller.addListener(() {
      print("Selected Index: " + _controller.index.toString());
    });

    // for (int i = 0; i < 12; i++) {
    //   Info info = new Info();
    //   itemscontroller.items({"ddddd": info});
    // }
  }
//   void showMyBottomSheet(BuildContext context) {

// }

  TextStyle addressTextStyle = Styles.customNormalTextStyle(
    color: AppColors.accentText,
    fontSize: Sizes.TEXT_SIZE_14,
  );

  TextStyle openingTimeTextStyle = Styles.customNormalTextStyle(
    color: Colors.red,
    fontSize: Sizes.TEXT_SIZE_14,
  );

  TextStyle subHeadingTextStyle = Styles.customTitleTextStyle(
    color: AppColors.headingText,
    fontWeight: FontWeight.w600,
    fontSize: Sizes.TEXT_SIZE_18,
  );

  BoxDecoration fullDecorations = Decorations.customHalfCurvedButtonDecoration(
    color: Colors.black.withOpacity(0.1),
    topleftRadius: 24,
    bottomleftRadius: 24,
    topRightRadius: 24,
    bottomRightRadius: 24,
  );
  BoxDecoration leftSideDecorations =
      Decorations.customHalfCurvedButtonDecoration(
    color: Colors.black.withOpacity(0.1),
    topleftRadius: 24,
    bottomleftRadius: 24,
  );

  BoxDecoration rightSideDecorations =
      Decorations.customHalfCurvedButtonDecoration(
    color: Colors.black.withOpacity(0.1),
    topRightRadius: 24,
    bottomRightRadius: 24,
  );

  @override
  Widget build(BuildContext context) {
    List<String> args = Get.arguments;
//    final RestaurantDetails args = ModalRoute.of(context).settings.arguments;
    var heightOfStack = MediaQuery.of(context).size.height / 2.8;
    var aPieceOfTheHeightOfStack = heightOfStack - heightOfStack / 3.5;
    return DefaultTabController(
      length: 1,
      child: Scaffold(
        body: SafeArea(
            top: true,
            left: false,
            right: false,
            child: NestedScrollView(
                controller: _scrollViewController,
                headerSliverBuilder:
                    (BuildContext context, bool innerBoxIsScrolled) {
                  return <Widget>[
                    SliverAppBar(
                      // toolbarHeight: ,
                      expandedHeight: 400.0,
                      backgroundColor: Colors.white,
                      pinned: true,
                      floating: false,
                      toolbarHeight: 100,
                      // snap: true,

                      // forceElevated: boxIsScrolled,
                      title: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            "txt",
                            //args[6].toString(),
                            style: TextStyle(color: Colors.black),
                          )),
                      leading: InkWell(
                        onTap: () => {Get.back()}, //AppRouter.navigator.pop(),
                        child: Padding(
                            padding: const EdgeInsets.only(
                              left: Sizes.MARGIN_16,
                              right: Sizes.MARGIN_16,
                            ),
                            child: Icon(
                              Icons.arrow_back_ios,
                              color: Colors.black,
                            )),
                      ),
                      actions: [
                        InkWell(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: iconLove,
                            ),
                            onTap: () {
                              iconLove ==
                                      Icon(
                                        FlutterIcons.favorite_border_mdi,
                                        color: Colors.red,
                                      )
                                  ? iconLove = Icon(
                                      FlutterIcons.favorite_mdi,
                                      color: Colors.red,
                                    )
                                  : iconLove = Icon(
                                      FlutterIcons.favorite_border_mdi,
                                      color: Colors.red,
                                    );
                            })
                      ],

                      // here the desired height

                      flexibleSpace: FlexibleSpaceBar(
                        background: Image.asset(
                          args[1].toString(),
                          fit: BoxFit.fitWidth,
                          height: 400,
                        ),
                      ),

                      bottom: PreferredSize(
                        preferredSize: Size.fromHeight(50),
                        child: Container(
                          child: Column(
                            children: [
                              Container(
                                padding:
                                    const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                height: 50,
                                child: Row(
                                  children: <Widget>[
                                    Text(
                                      args[0].toString(),
                                      textAlign: TextAlign.left,
                                      style: Styles.customTitleTextStyle(
                                        color: AppColors.headingText,
                                        fontWeight: FontWeight.w600,
                                        fontSize: Sizes.TEXT_SIZE_20,
                                      ),
                                    ),
                                    SizedBox(width: 4.0),
                                    CardTags(
                                      title: args[2],
                                      decoration: BoxDecoration(
                                        gradient: Gradients.secondaryGradient,
                                        boxShadow: [
                                          Shadows.secondaryShadow,
                                        ],
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(8.0)),
                                      ),
                                    ),
                                    SizedBox(width: 4.0),
                                    CardTags(
                                      title: args[5],
                                      decoration: BoxDecoration(
                                        color:
                                            Color.fromARGB(255, 132, 141, 255),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(8.0)),
                                      ),
                                    ),
                                    Spacer(flex: 1),
                                    Container(
                                      child: Row(
                                        children: [
                                          Image.asset(
                                            ImagePath.starIcon,
                                            height: Sizes.WIDTH_14,
                                            width: Sizes.WIDTH_14,
                                          ),
                                          Text(
                                            args[4].toString(),
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              TabBar(
                                labelStyle: TextStyle(fontSize: 15.0),
                                unselectedLabelStyle: TextStyle(fontSize: 12.0),
                                indicator: BoxDecoration(color: Colors.green[50]
                                    // color: _hasBeenPressed ? Color(0xffffffff) : Color(0xffff00a8),
                                    ),
                                unselectedLabelColor: Colors.green[50],
                                indicatorColor: Colors.green[50],
                                tabs: [
                                  Tab(
                                      child: Text(
                                    args[7].toString(),
                                    style: TextStyle(color: Colors.black),
                                  )),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                      // SliverPadding(
                      //   padding: new EdgeInsets.all(16.0),
                      //   sliver: new SliverList(
                      //     delegate: new SliverChildListDelegate([

                      //         ],
                      //       ),
                      //     ]),
                      //   ),
                    ),
                  ];
                },
                body: TabBarView(children: <Widget>[
                  setItemList(),
                ]))),
      ),
    );
  }

  List<String> foodNameAll = [
    "Kacci Biriyani",
    "Morog Polao",
    "Burger",
    "Pizza",
  ];
  List<int> presentPrice = [
    100,
    120,
    130,
    100,
    120,
    130,
    100,
    120,
    130,
    100,
    120,
    130,
    100,
    120,
  ];
  List<String> pastPrice = [
    "110",
    "130",
    "150",
    "110",
    "130",
    "150",
    "110",
    "130",
    "150",
    "110",
    "130",
    "150",
    "110",
    "130",
  ];
  List<int> _itemCount = [
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
  ];

  List<Widget> createUserListTiles({@required numberOfUsers}) {
    List<Widget> userListTiles = [];
    List<String> imagePaths = [
      ImagePath.profile1,
      ImagePath.profile4,
      ImagePath.profile3,
      ImagePath.profile4,
      ImagePath.profile1,
    ];
    List<String> userNames = [
      "Collin Fields",
      "Sherita Burns",
      "Bill Sacks",
      "Romeo Folie",
      "Pauline Cobbina",
    ];

    List<String> description = [
      "Lorem Ipsum baga fada",
      "Lorem Ipsum baga fada",
      "Lorem Ipsum baga fada",
      "Lorem Ipsum baga fada",
      "Lorem Ipsum baga fada",
    ];
    List<String> ratings = [
      "4.0",
      "3.0",
      "5.0",
      "2.0",
      "4.0",
    ];

    List<int> list = List<int>.generate(numberOfUsers, (i) => i + 1);

    list.forEach((i) {
      userListTiles.add(ListTile(
        leading: Image.asset(imagePaths[i - 1]),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              userNames[i - 1],
              style: subHeadingTextStyle,
            ),
            //Ratings(ratings[i - 1]),
          ],
        ),
        contentPadding: EdgeInsets.symmetric(horizontal: 0),
        subtitle: Text(
          description[i - 1],
          style: addressTextStyle,
        ),
      ));
    });
    return userListTiles;
  }
}

class MyBottomSheet {}
