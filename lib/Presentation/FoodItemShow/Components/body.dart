import 'package:flutter/material.dart';
import 'package:rocketMeal/Presentation/FoodItemShow/Components/itemshow.dart';

class Body extends StatefulWidget {
  final argmnt;
  Body({
    Key key,
    this.argmnt,
  }) : super(key: key);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  @override
  Widget build(BuildContext context) {
    return ItemShow(args: widget.argmnt);
  }
}
