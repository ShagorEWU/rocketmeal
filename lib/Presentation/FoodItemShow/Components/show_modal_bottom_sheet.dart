import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:rocketMeal/Common/Database/database_helper.dart';
import 'package:rocketMeal/Controller/bottomshow.dart';
import 'package:sqflite/sqflite.dart';

class itemList {
  String name;
  int price;
  int amount;
}

class ShowModalBottomSheet extends StatefulWidget {
  final name;
  ShowModalBottomSheet({Key key, this.name}) : super(key: key);

  @override
  _ShowModalBottomSheetState createState() => _ShowModalBottomSheetState();
}

class _ShowModalBottomSheetState extends State<ShowModalBottomSheet> {
  var argument = Get.arguments;
  List<itemList> newListt = [];
  List<Map<String, dynamic>> currentItem = [];

  listState() async {
    currentItem = await DatabaseHelper.instance.queryAll();
    if (currentItem.length == 0) {
      // Map<String, dynamic> row = {"name": "kacci", "price": 10.0, "amount": 10};

      // await DatabaseHelper.instance.insert(row);

    }
    print(currentItem.length);
    // currentItem.addAll(await DatabaseHelper.instance.queryAll());
    for (int i = 0; i < currentItem.length; i++) {
      itemList uuu = new itemList();
      uuu.amount = currentItem[i]["amount"];
      uuu.price = currentItem[i]["price"];
      uuu.name = currentItem[i]["name"];
      newListt.add(uuu);
    }
  }

  int _id = 0;

  insertValue() async {}

  @override
  initState() {
    listState();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DraggableScrollableSheet(
      expand: true,
      builder: (_, controller) {
        return Column(
          children: [
            Container(
              height: 50,
              margin: EdgeInsets.symmetric(vertical: 10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: Colors.grey,
              ),
            ),
            Expanded(
              child: ListView.builder(
                controller: controller,
                itemCount: newListt.length,
                itemBuilder: (_, i) => ListTile(
                    title: Row(
                  children: [
                    IconButton(
                        icon: Icon(Icons.remove),
                        onPressed: () {
                          // print(makeModifiableResults);

                          setState(() {
                            print(newListt);
                            print("DttD");
                            // int ssss = 1;

                            // //currentItem[i]["amount"];
                            // print("DttD");
                            // ssss = ssss + 1;
                            print(newListt[i].name);
                            print(newListt.length);
                            newListt[i].amount--;
                            print("DttDD");
                            // currentItem[i]['name'] = currentItem[i]['name'] + 1;
                            print(newListt[i].amount);

                            //print(ssss);

                            if (newListt[i].amount == 0) {
                              String txt = newListt[i].name;
                              newListt.removeAt(i);
                              DatabaseHelper.instance.delete(txt);
                            } else {
                              // print(currentItem[i]['amount']);
                              Map<String, dynamic> map = {
                                "name": newListt[i].name,
                                "price": newListt[i].price,
                                "amount": newListt[i].amount
                              };
                              DatabaseHelper.instance.update(
                                map,
                              );
                              print(map['amount']);
                            }
                          });
                        }),
                    Text(
                        "  $i  ${newListt[i].amount}   and  ${newListt[i].name}"),
                    IconButton(
                        icon: Icon(Icons.add),
                        onPressed: () {
                          setState(() {
                            print(newListt);
                            print("DttD");
                            // int ssss = 1;

                            // //currentItem[i]["amount"];
                            // print("DttD");
                            // ssss = ssss + 1;
                            print(newListt[i].name);
                            // print(newListt.length);
                            newListt[i].amount++;
                            print("DttD");
                            // currentItem[i]['name'] = currentItem[i]['name'] + 1;
                            print(newListt[i].amount);

                            print(newListt[i].amount + 100);

                            if (newListt[i].amount == 0) {
                              String txt = newListt[i].name;
                              newListt.removeAt(i);
                              DatabaseHelper.instance.delete(txt);
                            } else {
                              // print(currentItem[i]['amount']);
                              Map<String, dynamic> map = {
                                "name": newListt[i].name,
                                "price": newListt[i].price,
                                "amount": newListt[i].amount
                              };
                              DatabaseHelper.instance.update(
                                map,
                              );
                              print(map['amount']);
                            }
                            //   DatabaseHelper.instance
                            //       .delete(currentItem[i]['name']);
                            // }

                            // icontroller.bottomShow.value.height.value =
                            //     currentItem[i]['amount'];
                          });
                        })
                  ],
                )),
              ),
            ),
            RaisedButton(
                child: Text("Submit"),
                onPressed: () {
                  setState(() {});

                  //print(icontroller.bottomShow.value.val.value);
                })
          ],
        );
      },
    );
  }
}
