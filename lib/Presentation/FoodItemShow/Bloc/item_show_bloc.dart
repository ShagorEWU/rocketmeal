import 'package:get/get.dart';
import 'package:rocketMeal/Common/Database/database_helper.dart';

class itemList {
  String name;
  int price;
  int amount;
  itemList(this.name, this.price, this.amount);
}

class Todo {
  String name;
  int price;
  int amount;

  Todo(this.name, this.price, this.amount);
}

Map<String, dynamic> customListtt = {"name": "", "price": 0, "amount": 0};

class ItemShowController extends GetxController {
  ItemShowController();

  final customList = <Map<String, dynamic>>[].obs;

  Future<List<Map<String, dynamic>>> generateCustomList(
      List newListTemp) async {
    //print(" length = ${newListTemp.length}");
    for (int i = 0; i < newListTemp.length; i++) {
      int amountTemp =
          await DatabaseHelper.instance.querySingleRow(newListTemp[i].name);

      if (amountTemp > 0) {
        Map<String, dynamic> myMap = {
          "name": newListTemp[i].name,
          "price": newListTemp[i].price,
          "amount": amountTemp
        };
        customList.add(myMap);
      } else {
        customList.add({
          "name": newListTemp[i].name,
          "price": newListTemp[i].price,
          "amount": 0
        });
      }
    }

    return customList;
  }
}
