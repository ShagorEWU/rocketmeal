import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:rocketMeal/Presentation/FoodMenuShow/Components/body.dart';

class FoodMenuShowScreen extends StatefulWidget {
  FoodMenuShowScreen({Key key}) : super(key: key);

  @override
  _FoodMenuShowScreenState createState() => _FoodMenuShowScreenState();
}

class _FoodMenuShowScreenState extends State<FoodMenuShowScreen> {
  String args = Get.arguments;
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        child: Scaffold(
            body: Body(
          category: args.toString(),
        )),
      ),
    );
  }
}
