import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:rocketMeal/Presentation/FoodMenuShow/Components/food_menu.dart';

class Body extends StatefulWidget {
  final category;
  Body({Key key, this.category}) : super(key: key);
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  bool showStatusBar = true;
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<List<SystemUiOverlay>>(
      value: showStatusBar ? SystemUiOverlay.values : [],
      child: AnnotatedRegion<SystemUiOverlayStyle>(
        value: Platform.isAndroid
            ? SystemUiOverlayStyle.light.copyWith(
                statusBarIconBrightness: Brightness.dark,
                statusBarColor: showStatusBar ? Colors.white : Colors.orange,
                systemNavigationBarColor: Colors.black12,
                systemNavigationBarIconBrightness: Brightness.dark,
              )
            : SystemUiOverlayStyle.dark.copyWith(
                statusBarIconBrightness: Brightness.light,
                statusBarColor: showStatusBar ? Colors.white : Colors.orange,
                systemNavigationBarColor: Colors.black12,
                systemNavigationBarIconBrightness: Brightness.light,
              ),
        child: SafeArea(
          top: true,
          bottom: false,
          right: false,
          left: false,
          child: Scaffold(
            appBar: PreferredSize(
              preferredSize: Size.fromHeight(60.0),
              child: AppBar(
                leading: IconButton(
                    icon: Icon(
                      Icons.arrow_back_ios,
                      color: Colors.black,
                    ),
                    onPressed: () {
                      Get.back();
                    }),
                title: Text(
                  widget.category.toString(),
                  style: TextStyle(fontSize: 20, color: Colors.black),
                ),
                backgroundColor: Colors.white,
              ),
            ),
            body: FoodMenu(category: widget.category),
          ),
        ),
      ),
    );
  }
}
