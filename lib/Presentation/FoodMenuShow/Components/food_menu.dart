import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:rocketMeal/Presentation/SpecificItemResturant/specific_item_resturants.dart';
import 'package:rocketMeal/Router/routeName.dart';
import 'package:rocketMeal/values/data.dart';

class FoodMenu extends StatefulWidget {
  final category;
  FoodMenu({Key key, this.category}) : super(key: key);

  @override
  _FoodMenuState createState() => _FoodMenuState();
}

class _FoodMenuState extends State<FoodMenu> {
  int widthOfImage;
  int indexNo = 0;
  @override
  void initState() {
    //widthOfImage = MediaQuery.of(context).size.width;
    if (widget.category == "Launch") {
      indexNo = 1;
    } else if (widget.category == "Dinner") {
      indexNo = 2;
    }
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        shrinkWrap: true,
        itemCount: foodMenuItems[indexNo].length,
        itemBuilder: (context, index) {
          return Column(children: [
            SizedBox(
              height: 10,
            ),
            InkWell(
              onTap: () {
                Get.toNamed(
                  RouteName.specificItemResturantsScreen,
                  arguments: foodMenuItems[indexNo][index].toString(),
                );
              },
              child: Container(
                // padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                width: MediaQuery.of(context).size.width,
                height: 100,
                child: Stack(children: <Widget>[
                  Positioned(
                    left: 10,
                    right: 10,
                    bottom: 0,
                    top: 0,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: Image.asset(
                        imagePathsNew[index],
                        // width: 300,
                        height: 100,
                        width: MediaQuery.of(context).size.width,
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                  ),
                  Positioned(
                    left: 10,
                    right: 10,
                    child: Opacity(
                      opacity: 0.7,
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: 100,
                        decoration: BoxDecoration(
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                                color: Colors.grey[800].withOpacity(0.9),
                                blurRadius: 4.0,
                                offset: Offset(3.0, 3))
                          ],
                          gradient: gradients[index],
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ),
                  ),
                  Center(
                    // alignment: Alignment.center,
                    child: Text(
                      "${foodMenuItems[indexNo][index]}",
                      style: TextStyle(
                          color: Colors.white, wordSpacing: 2, fontSize: 20),
                    ),
                  ),
                ]),
              ),
            ),
            SizedBox(
              height: 10,
            ),
          ]);
        });
  }
}
