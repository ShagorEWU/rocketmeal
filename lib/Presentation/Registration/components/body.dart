import 'dart:collection';
import 'dart:math' as math;
import 'package:rocketMeal/Common/Methods/CommonMethod.dart';
import 'package:rocketMeal/Common/Variables/TextList.dart';
import 'package:rocketMeal/Presentation/Registration/Block/registration_bloc.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import 'MyAppSpace.dart';
import 'registration_form_card.dart';

import '../../../Common/Widgets/Button/radioButton.dart';

// ignore: camel_case_types
class body extends StatefulWidget {
  body({Key key}) : super(key: key);

  @override
  _bodyState createState() => _bodyState();
}

// ignore: camel_case_types
class _bodyState extends State<body> with SingleTickerProviderStateMixin {
  ScrollController _scrollViewController;
  TextEditingController userName = new TextEditingController();
  TextEditingController phoneNumber = new TextEditingController();
  TextEditingController email = new TextEditingController();
  TextEditingController password = new TextEditingController();
  TextEditingController address = new TextEditingController();
  TextEditingController serviceArea = new TextEditingController();
  TextEditingController passWord = new TextEditingController();
  TextEditingController confirmPassWord = new TextEditingController();
  TextEditingController occupation = new TextEditingController();
  bool _isReistred;
  final RegistrationBloc _registrationBloc = Get.put(RegistrationBloc());
  CommonMethod commonMethod = Get.find();
  ScrollController _controller;
  bool silverCollapsed = false;
  String myTitle = "default title";

  @override
  void initState() {
    super.initState();

    _controller = ScrollController();

    _controller.addListener(() {
      if (_controller.offset > 220 && !_controller.position.outOfRange) {
        if (!silverCollapsed) {
          // do what ever you want when silver is collapsing !

          myTitle = "silver collapsed !";
          silverCollapsed = true;
          setState(() {});
        }
      }
      if (_controller.offset <= 220 && !_controller.position.outOfRange) {
        if (silverCollapsed) {
          // do what ever you want when silver is expanding !

          myTitle = "silver expanded !";
          silverCollapsed = false;
          setState(() {});
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    bool _isSelected = false;

    void _radio() {
      setState(() {
        _isSelected = !_isSelected;
      });
    }

    return ScreenUtilInit(
      designSize: Size(360, 690),
      allowFontScaling: false,
      child: new Scaffold(
        backgroundColor: Colors.white,
        resizeToAvoidBottomPadding: true,
        body: SafeArea(
          left: false,
          top: true,
          bottom: false,
          right: false,
          child: NestedScrollView(
            controller: _scrollViewController,
            headerSliverBuilder: (BuildContext context, bool boxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                    leading: IconButton(
                        icon: Icon(Icons.arrow_back),
                        onPressed: () {
                          Get.back();
                        }),
                    expandedHeight: 300.0,
                    floating: true,
                    pinned: true,
                    elevation: 1,
                    backgroundColor: Colors.white,
                    iconTheme: IconThemeData(
                      color: Colors.black45, //change your color here
                    ),
                    flexibleSpace: MyAppSpace())
              ];
            },
            body: SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.only(left: 40.0, right: 40.0, top: 0.0),
                child: Column(
                  children: <Widget>[
                    RegistrationFormCard(
                        UserName: userName,
                        PhoneNumber: phoneNumber,
                        Email: email,
                        Password: password,
                        Address: address,
                        ServiceArea: serviceArea,
                        PassWord: passWord,
                        ConfirmPassWord: confirmPassWord,
                        Occupation: occupation),
                    Row(
                      children: <Widget>[
                        Padding(padding: const EdgeInsets.only(left: 30)),
                        GestureDetector(
                          onTap: _radio,
                          child: radioButton(_isSelected),
                        ),
                        CustomText.textTermsCondition,
                      ],
                    ), //Login Form Card
                  ],
                ),
              ),
            ),
          ),
        ),
        bottomNavigationBar: SizedBox(
          height: ScreenUtil().setHeight(50), // specific value
          child: RaisedButton(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20.0),
                    topRight: Radius.circular(20.0)),
                side: BorderSide(color: Colors.green)),
            onPressed: () {
              //print(_loginBloc.emailTextController.text);
              Map<String, dynamic> map = new HashMap();
              map["adminName"] = userName.text.toString();
              map["phoneNumber"] = phoneNumber.text.toString();
              map["adminPassword"] = password.text.toString();
              map["email"] = email.text.toString();

              _registrationBloc.submitRegistrationData(map);
              Get.toNamed("/login");
            },
            color: Colors.green,
            textColor: Colors.white,
            child: Text('Registration'),

            //

            //
          ),
        ),
      ),
    );
  }
}
