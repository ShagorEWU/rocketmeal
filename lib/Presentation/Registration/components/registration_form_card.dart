import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:rocketMeal/Common/Widgets/Others/InputText.dart';

class RegistrationFormCard extends StatefulWidget {
  const RegistrationFormCard(
      {Key key,
      // ignore: non_constant_identifier_names
      @required this.UserName,
      // ignore: non_constant_identifier_names
      @required this.PhoneNumber,
      // ignore: non_constant_identifier_names
      @required this.Email,
      // ignore: non_constant_identifier_names
      @required this.Password,
      // ignore: non_constant_identifier_names
      @required this.Address,
      // ignore: non_constant_identifier_names
      @required this.ServiceArea,
      // ignore: non_constant_identifier_names
      @required this.PassWord,
      // ignore: non_constant_identifier_names
      @required this.ConfirmPassWord,
      // ignore: non_constant_identifier_names
      @required this.Occupation})
      : super(key: key);

  // ignore: non_constant_identifier_names
  final TextEditingController UserName;
  // ignore: non_constant_identifier_names
  final TextEditingController PhoneNumber;
  // ignore: non_constant_identifier_names
  final TextEditingController Email;
  // ignore: non_constant_identifier_names
  final TextEditingController Password;
  // ignore: non_constant_identifier_names
  final TextEditingController Address;
  // ignore: non_constant_identifier_names
  final TextEditingController ServiceArea;
  // ignore: non_constant_identifier_names
  final TextEditingController PassWord;
  // ignore: non_constant_identifier_names
  final TextEditingController ConfirmPassWord;
  // ignore: non_constant_identifier_names
  final TextEditingController Occupation;
  @override
  _RegistrationFormCardState createState() => _RegistrationFormCardState();
}

class _RegistrationFormCardState extends State<RegistrationFormCard> {
  bool _isSelected = false;

  void _radio() {
    setState(() {
      _isSelected = !_isSelected;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      width: double.infinity,
//      height: ScreenUtil.getInstance().setHeight(500),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8.0),
          boxShadow: [
            BoxShadow(
                color: Colors.white,
                offset: Offset(0.0, 15.0),
                blurRadius: 15.0),
            BoxShadow(
                color: Colors.white,
                offset: Offset(0.0, -10.0),
                blurRadius: 10.0),
          ]),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: ScreenUtil().setHeight(65),
            child: InputText(txt: "username", textController: widget.UserName),
          ),
          Padding(padding: const EdgeInsets.only(top: 20.0)),
          SizedBox(
            height: ScreenUtil().setHeight(65),
            child: InputText(
                txt: "phoneNumber", textController: widget.PhoneNumber),
          ),
          Padding(padding: const EdgeInsets.only(top: 20.0)),
          SizedBox(
            height: ScreenUtil().setHeight(65),
            child: InputText(txt: "email", textController: widget.Email),
          ),
          Padding(padding: const EdgeInsets.only(top: 20.0)),
          SizedBox(
            height: ScreenUtil().setHeight(65),
            child:
                InputText(txt: "occupation", textController: widget.Occupation),
          ),
          Padding(padding: const EdgeInsets.only(top: 20.0)),
          SizedBox(
            height: ScreenUtil().setHeight(65),
            child: InputText(txt: "address", textController: widget.Address),
          ),
          Padding(padding: const EdgeInsets.only(top: 20.0)),
          SizedBox(
            height: ScreenUtil().setHeight(65),
            child: InputText(
                txt: "service area", textController: widget.ServiceArea),
          ),
          Padding(padding: const EdgeInsets.only(top: 20.0)),
          SizedBox(
            height: ScreenUtil().setHeight(65),
            child: InputText(txt: "password", textController: widget.Password),
          ),
          Padding(padding: const EdgeInsets.only(top: 20.0)),
          SizedBox(
            height: ScreenUtil().setHeight(65),
            child: InputText(
                txt: "Confirmspassword",
                textController: widget.ConfirmPassWord),
          ),
          Padding(padding: const EdgeInsets.only(top: 20.0)),
        ],
      ),
    );
  }
}
