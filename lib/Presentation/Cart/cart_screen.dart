import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:rocketMeal/Common/Widgets/BottomNavBar/bottomNavBar.dart';

import 'Components/body.dart';

class CartScreen extends StatefulWidget {
  static int index = 3;
  @override
  _CartScreenState createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  @override
  void initState() {
    // Timer.periodic(Duration(seconds: 5), (timer) {
    //   print(DateTime.now());
    // });
    // Timer.periodic(Duration(seconds: 100), (timer) {
    //   print(DateTime.now());
    // });
    // Timer.periodic(Duration(seconds: 100), (timer) {
    //   print(DateTime.now());
    // });
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      bottom: false,
      right: false,
      left: false,
      child: Scaffold(
        body: Body(),
        bottomNavigationBar: BottomNavBar(
          activeBar: CartScreen.index,
        ),
      ),
    );
  }
}
