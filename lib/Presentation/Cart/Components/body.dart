import 'package:flutter/material.dart';
import 'package:rocketMeal/values/data.dart';

import 'my_separator.dart';

class Body extends StatefulWidget {
  Body({Key key}) : super(key: key);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  List<int> amount = new List(20);
  int cnt = 0;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    amount[0] = 2;
    amount[1] = 1;
    amount[2] = 2;
    amount[3] = 2;
    amount[4] = 1;
    amount[5] = 2;
    amount[6] = 7;
    amount[9] = 1;
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: ScrollPhysics(),
      child: Column(
        children: [
          SizedBox(
            height: 20,
          ),
          Card(
            elevation: 1,
            color: Colors.lightGreen[100],
            child: Text(
              restaurantNames[0],
              style: TextStyle(fontSize: 30, color: Colors.black),
            ),
          ),
          Container(
            height: 2,
            color: Colors.white24,
          ),
          Row(
            children: [
              Expanded(
                child: ListView.builder(
                  shrinkWrap: true,
                  physics: ScrollPhysics(),
                  itemCount: 4,
                  itemBuilder: (context, index) {
                    return Row(
                      children: [
                        Container(
                          width: (MediaQuery.of(context).size.width -
                              (MediaQuery.of(context).size.width / 3) -
                              5),
                          child: Column(
                            children: [
                              amount[index] == 0
                                  ? {
                                      amount.removeAt(index),
                                      price.removeAt(index),
                                      amount.removeAt(index)
                                    }
                                  : Row(
                                      children: [
                                        IconButton(
                                            icon: Icon(Icons.remove_circle),
                                            onPressed: () {
                                              --amount[index];
                                            }),
                                        Text("${amount[index] * price[index]}"),
                                        IconButton(
                                            icon: Icon(Icons.add),
                                            onPressed: () {
                                              ++amount[index];
                                            }),
                                        SizedBox(height: 10, width: 10),
                                        Container(
                                            alignment: Alignment.centerRight,
                                            child: Text(
                                                "TK ${amount[index] * price[index]}"))
                                      ],
                                    )
                            ],
                          ),
                        )
                      ],
                    );
                  },
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

List<String> names = [
  "Camosa",
  "Murgi Parota",
  "Singara",
  "Kacci",
  "Gril",
  "Nan",
];
List<int> price = [
  10,
  20,
  10,
  100,
  100,
  20,
];
