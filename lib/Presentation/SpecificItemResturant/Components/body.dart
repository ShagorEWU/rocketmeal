import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:rocketMeal/Presentation/FoodItemShow/item_detail_screen.dart';
import 'package:rocketMeal/Presentation/Home/Components/foody_bite_card.dart';
import 'package:rocketMeal/Router/routeName.dart';
import 'package:rocketMeal/values/data.dart';

class Body extends StatefulWidget {
  final argss;
  Body({Key key, this.argss}) : super(key: key);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  List<String> argument = new List(8);
  //String gtatgs = Get.arguments;
  @override
  Widget build(BuildContext context) {
    return Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: ListView.builder(
            itemCount: 3,
            itemBuilder: (context, index) {
              return Container(
                margin: EdgeInsets.only(right: 4.0),
                child: FoodyBiteCard(
                  onTap: () {
                    setState(() {
                      argument[0] = restaurantNames[index];

                      argument[1] = imagePaths[index];
                      argument[2] = category[index];

                      argument[3] = addresses[index];
                      argument[4] = ratings[index];

                      argument[5] = distance[index];
                      argument[6] = status[index];
                      argument[7] = widget.argss.toString();
                    });
                    Get.toNamed(RouteName.itemDetailScreen,
                        arguments: argument);
                  },
                  imagePath: imagePathsNew[index],
                  status: status[index],
                  cardTitle: restaurantNames[index],
                  rating: ratings[index],
                  category: category[index],
                  distance: distance[index],
                  address: addresses[index],
                ),
              );
            }));
  }
}
