import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'Components/body.dart';

class SpecificItemResturantsScreen extends StatefulWidget {
  SpecificItemResturantsScreen({Key key}) : super(key: key);

  @override
  _SpecificItemResturantsScreenState createState() =>
      _SpecificItemResturantsScreenState();
}

class _SpecificItemResturantsScreenState
    extends State<SpecificItemResturantsScreen> {
  @override
  void initState() {
    // TODO: implement initState

    print("SDFSS");
  }

  @override
  Widget build(BuildContext context) {
    var args = Get.arguments;
    return SafeArea(
      top: true,
      bottom: false,
      right: false,
      left: false,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                args.toString(),
                style: TextStyle(color: Colors.black),
              )),
          leading: InkWell(
            onTap: () => {Get.back()}, //AppRouter.navigator.pop(),
            child: Padding(
                padding: const EdgeInsets.only(
                  left: 16,
                  right: 16,
                ),
                child: Icon(
                  Icons.arrow_back_ios,
                  color: Colors.black,
                )),
          ),
        ),
        body: Body(argss: args),
      ),
    );
  }
}
