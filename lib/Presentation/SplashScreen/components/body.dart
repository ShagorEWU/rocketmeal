import 'package:flutter/material.dart';
import 'dart:async';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:rocketMeal/Router/routeName.dart';
import 'package:rocketMeal/Router/routes.dart';

// ignore: camel_case_types
class body extends StatefulWidget {
  body({Key key}) : super(key: key);

  @override
  _bodyState createState() => _bodyState();
}

// ignore: camel_case_types
class _bodyState extends State<body> {
  @override
  void initState() {
    super.initState();
    Timer(Duration(seconds: 4), () {
      Get.offNamed(RouteName.onBoardScreen);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: WillPopScope(
            // ignore: missing_return
            onWillPop: () {},
            child: Container(
              color: Colors.white,
              child: SimpleDialog(
                elevation: 0.0,
                backgroundColor: Colors.white,
                children: <Widget>[
                  Center(
                      child: Container(
                          child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        child: Image.asset(
                          'asset/rocketSplash.jpg',
                          height: 150,
                          width: 150,
                        ),
                      )
                    ],
                  ))),
                ],
              ),
            )),
      ),
      bottomNavigationBar: Container(
        color: Colors.white,
        height: 100,
        child: Center(
          child: Text(
            "Rocket",
            style: TextStyle(
              fontSize: 20,
              color: Colors.black54,
            ),
          ),
        ),
      ),
    );
  }
}
