import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:rocketMeal/Animation/fadeAnimationFall.dart';
import 'package:rocketMeal/Animation/fadeAnimationSlide2Left.dart';
import 'package:rocketMeal/Common/Methods/HexColor.dart';
import 'package:rocketMeal/Common/Methods/SplashEffect.dart';
import 'package:rocketMeal/Router/routeName.dart';
import 'package:rocketMeal/Router/routes.dart';

import 'otp_form_card.dart';

// ignore: camel_case_types
class body extends StatefulWidget {
  @override
  _bodyState createState() => new _bodyState();
}

// ignore: camel_case_types
class _bodyState extends State<body> {
  bool _isSelected = false;

  void _radio() {
    setState(() {
      _isSelected = !_isSelected;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomPadding: true,
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.only(left: 28.0, right: 28.0, top: 60.0),
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(top: 00.0),
                    child: Image.asset("asset/login.jpg"),
                  ),

                  otp_form_card(), //Reset Form Card
                ],
              ),
            ),
          )
        ],
      ),
      bottomNavigationBar: SizedBox(
        height: ScreenUtil().setHeight(50), // specific value
        child: RaisedButton(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20.0),
                  topRight: Radius.circular(20.0)),
              side: BorderSide(color: Colors.green)),
          onPressed: () {
            //print(_loginBloc.emailTextController.text);

            Get.toNamed(RouteName.homeScreen);
          },
          color: Colors.green,
          textColor: Colors.white,
          child: Text('Submit'),

          //

          //
        ),
      ),
    );
  }
}
