import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'Components/body.dart';

// ignore: camel_case_types
class OTPScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        body: body(),
      ),
    );
  }

  Future<bool> _onBackPressed() {
    return Get.defaultDialog(
        //context: context,

        title: 'Are you sure?',
        content: new Text('Do you want to exit an App'),
        actions: <Widget>[
          new GestureDetector(
            onTap: () => Get.toNamed("/otp"),
            child: Text("NO"),
          ),
          SizedBox(height: 16),
          new GestureDetector(
            onTap: () => Get.back(),
            child: Text("YES"),
          ),
        ]);
  }
}
