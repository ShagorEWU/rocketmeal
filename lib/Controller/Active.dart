import 'package:get/get.dart';

class Active {
  final status = true.obs;
}

class ActiveController extends GetxController {
  ActiveController();

  final active = Active().obs;
}
