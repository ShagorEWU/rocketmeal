import 'package:get/get.dart';

class User {
  final userName = "".obs;
  final phoneNumber = "".obs;
  final email = "".obs;
}

class UserController extends GetxController {
  UserController();

  final user = User().obs;
}
