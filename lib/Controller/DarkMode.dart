import 'package:get/get.dart';

class DarkMode {
  final status = true.obs;
}

class DarkModeController extends GetxController {
  DarkModeController();

  final darkMode = DarkMode().obs;
}
