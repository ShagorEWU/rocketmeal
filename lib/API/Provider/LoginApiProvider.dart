import 'dart:async';
import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:rocketMeal/Model/loginModel/LoginModel.dart';

import '../ApiConf.dart';

class LoginApiProvider {
  Dio dio;
  ApiConf apiConf = new ApiConf();

  LoginApiProvider() {
    dio = apiConf.getDio();
  }

  Future checkLogin(Map<String, dynamic> map) async {
    // try {
    Response response =
        await dio.post('post/read.php?action=adminAuthentication', data: map);
    print(response.data);
    return LoginModel.fromJson(response.data);
    // } //The model class returns data with json.decode().
    // on DioError catch (error, stacktrace) {
    //   print("Exception occured: $error stackTrace: $stacktrace");
    //   return LoginModel.withError(apiConf.handleError(error));
    // }
  }
}
