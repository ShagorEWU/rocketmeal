import 'package:get/get.dart';

import 'package:rocketMeal/API/Provider/RegistrationApiProvider.dart';

class RegistrationRepository {
  RegistrationApiProvider registrationApiProvider = Get.find();

  checkRegistration(Map<String, dynamic> map) {
    return registrationApiProvider.checkRegistration(map);
  }
}
