import 'package:get/get.dart';
import 'package:rocketMeal/API/Provider/LoginApiProvider.dart';
import 'package:rocketMeal/Model/loginModel/LoginModel.dart';

class LoginRepository {
  LoginApiProvider loginApiProvider = Get.find();

  checkLogin(Map<String, dynamic> map) {
    return loginApiProvider.checkLogin(map);
  }
}
