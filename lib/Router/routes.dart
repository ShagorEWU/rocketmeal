import 'package:rocketMeal/Presentation/Cart/cart_screen.dart';
import 'package:rocketMeal/Presentation/FoodItemShow/item_detail_screen.dart';
import 'package:rocketMeal/Presentation/FoodMenuShow/food_menu_show_screen.dart';
import 'package:rocketMeal/Presentation/Home/home_scren.dart';
import 'package:rocketMeal/Presentation/Profile/components/account_information.dart';
import 'package:rocketMeal/Presentation/ResturantDetailScreen/restaurant_details_screen.dart';
import 'package:rocketMeal/Presentation/SpecificItemResturant/specific_item_resturants.dart';
import 'package:rocketMeal/Router/routeName.dart';
import 'package:get/get.dart';

import '../Common/Widgets/BottomNavBar/bottomNavBar.dart';
import '../Presentation/Login/login_screen.dart';

import '../Presentation/OTP/otp_screen.dart';
import '../Presentation/OnBoardScreen/on_board_screen.dart';

import '../Presentation/Registration/registration_screen.dart';
import '../Presentation/ResetPassword/reset_password_screen.dart';
import '../Presentation/SplashScreen/splash_screen.dart';
import '../Presentation/profile/profile_screen.dart';

class Routes {
  static final route = [
    GetPage(
      name: RouteName.splashScreen,
      page: () => SplashScreen(),
    ),
    GetPage(
      name: RouteName.loginScreen,
      page: () => LoginScreen(),
    ),
    GetPage(
      name: RouteName.registrationScreen,
      page: () => RegistrationScreen(),
    ),
    GetPage(
      name: RouteName.resetPasswordScreen,
      page: () => ResetPasswordScreen(),
    ),
    GetPage(
      name: RouteName.optScreen,
      page: () => OTPScreen(),
    ),
    GetPage(
      name: RouteName.homePageScreen,
      page: () => BottomNavBar(
        activeBar: 0,
      ),
    ),
    GetPage(
      name: RouteName.profileScreen,
      page: () => ProfileScreen(),
    ),
    GetPage(
      name: RouteName.onBoardScreen,
      page: () => OnBoardScreen(),
    ),
    GetPage(
      name: "/accountInformation",
      page: () => AccountInformation(),
    ),
    GetPage(
      name: RouteName.homeScreen,
      page: () => HomeScreen(),
    ),
    GetPage(
      name: RouteName.resturantDetailsScreen,
      page: () => ResturantDetailsScreen(),
    ),
    GetPage(
      name: RouteName.foodMenuShowScreen,
      page: () => FoodMenuShowScreen(),
    ),
    GetPage(
      name: RouteName.itemDetailScreen,
      page: () => ItemDetailScreen(),
    ),
    GetPage(
      name: RouteName.specificItemResturantsScreen,
      page: () => SpecificItemResturantsScreen(),
    ),
    GetPage(
      name: RouteName.cartScreen,
      page: () => CartScreen(),
    ),
  ];
}
