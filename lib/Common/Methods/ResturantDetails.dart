class RestaurantDetails {
  final String imagePath;
  final String restaurantName;
  final String restaurantAddress;
  final String category;
  final String distance;
  final String rating;

  RestaurantDetails({
    this.restaurantName,
    this.imagePath,
    this.category,
    this.restaurantAddress,
    this.rating,
    this.distance,
  });
}
