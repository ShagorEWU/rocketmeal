import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'HexColor.dart';

class SplashEffect extends StatelessWidget {
  final Widget child;
  final Function onTap;
  final BorderRadius borderRadius;
  final String splashColor, bgColor;

  const SplashEffect(
      {Key key,
      this.child,
      this.onTap,
      this.borderRadius,
      this.bgColor,
      this.splashColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: bgColor != null ? HexColor(bgColor) : Colors.transparent,
      child: InkWell(
        splashColor:
            splashColor != null ? HexColor(splashColor) : HexColor("#474747"),
        borderRadius:
            borderRadius != null ? borderRadius : BorderRadius.circular(0.0),
        child: child,
        onTap: onTap,
      ),
    );
  }
}
