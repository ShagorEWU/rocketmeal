import 'package:intl/intl.dart';

class CommonMethod {
  CommonMethod();

  DateTime dateTimeFromMilSec(int data) {
    return DateTime.fromMillisecondsSinceEpoch(data);
  }

  double stringToDouble(String data) {
    double myDouble = double.parse(data);
    assert(myDouble is double);
    return myDouble;
  }

  int stringToInt(String data) {
    int myInt = int.parse(data);
    assert(myInt is int);
    return myInt;
  }

  String formatDateTime(DateTime dateTime, String format) {
    var formatter = new DateFormat(format);
    String formattedDateTime = formatter.format(dateTime);
    return formattedDateTime;
  }

  String wordFirstCharacterUpper(String text) {
    List<dynamic> arrayPieces = [];

    String outPut = '';

    text.split(' ').forEach((sepparetedWord) {
      arrayPieces.add(sepparetedWord);
    });

    arrayPieces.forEach((word) {
      word =
          "${word[0].toString().toUpperCase()}${word.toString().substring(1)} ";
      outPut += word;
    });

    return outPut;
  }
}
