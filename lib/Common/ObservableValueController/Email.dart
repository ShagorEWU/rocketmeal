import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:rocketMeal/Common/Methods/HexColor.dart';

class EmailController extends GetxController {
  final email = "".obs;
  final status = false.obs;
  final colors = "#249B96".obs;
}
