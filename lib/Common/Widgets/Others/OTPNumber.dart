import 'package:flutter/material.dart';

class InputOtpNumber extends StatefulWidget {
  InputOtpNumber({Key key}) : super(key: key);

  @override
  _InputOtpNumberState createState() => _InputOtpNumberState();
}

class _InputOtpNumberState extends State<InputOtpNumber> {
  FocusNode myFocusNode;

  @override
  void initState() {
    super.initState();

    myFocusNode = FocusNode();
  }

  @override
  void dispose() {
    // Clean up the focus node when the Form is disposed.
    myFocusNode.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: new Expanded(
        child: new Padding(
          padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
          child: new TextField(
            //focusNode: myFocusNode,
            decoration: new InputDecoration(
              filled: true,
            ),
          ),
        ),
      ),
    );
  }
}
