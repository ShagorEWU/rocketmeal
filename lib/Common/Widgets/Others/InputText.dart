import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:rocketMeal/Common/Methods/HexColor.dart';
import 'package:rocketMeal/Common/ObservableValueController/Password.dart';

class InputText extends StatefulWidget {
  final String txt;
  final TextEditingController textController;
  InputText({Key key, @required this.txt, @required this.textController})
      : super(key: key);

  @override
  _InputTextState createState() => _InputTextState();
}

class _InputTextState extends State<InputText> {
  final formKey = GlobalKey<FormState>();
  final scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormFieldState> _inputValueFormKey =
      GlobalKey<FormFieldState>();

  final passwordcontroller = Get.put(PasswordController());
  // final inputboxoutlinecolor = Get.put(InputBoxOutLineColorController());
  //For Keyboard
  FocusNode myFocusNode;
  //For show text
  bool _obsecureText = false;
  @override
  void initState() {
    //emailcontroller.user.color.value = HexColor("#249B96");
    super.initState();
    setState(() {
      // if(widget.txt==)
      if (widget.txt == "password" || widget.txt == "Confirmpassword") {
        _obsecureText = true;
      }
    });

    myFocusNode = FocusNode();
  }

  @override
  void dispose() {
    // Clean up the focus node when the Form is disposed.
    myFocusNode.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 55,
      width: MediaQuery.of(context).size.width - 56.0,
      child: TextFormField(
        obscureText: _obsecureText,
        controller: widget.textController,
        key: _inputValueFormKey,
        keyboardType: TextInputType.emailAddress,
        decoration: new InputDecoration(
          labelText: widget.txt,
          border: new OutlineInputBorder(
            borderRadius: new BorderRadius.circular(10.0),
            borderSide: new BorderSide(),
          ),
        ),
        onChanged: (value) {
          setState(() {
            if (widget.txt == "password") {
              passwordcontroller.password.value = value;
            }
            _inputValueFormKey.currentState.validate();
          });
        },
        validator: (value) {
          if (widget.txt == "email" && value.length > 0) {
            if (GetUtils.isEmail(value) == true) {
              return null;
            } else {
              return "${widget.txt} is not Valid";
            }
          } else if (widget.txt == "phoneNumber" && value.length > 0) {
            String patttern = r'(^\+?(88)?0?1[3456789][0-9]{8}\b)';
            RegExp regExp = new RegExp(patttern);
            if (!regExp.hasMatch(value)) {
              return "${widget.txt} is not Valid";
            } else {
              return null;
            }
          } else if (widget.txt == "Confirmspassword") {
            if (passwordcontroller.password.value == value) {
              return null;
            } else {
              return "Password not match";
            }
          }

          return null;
        },
      ),
    );
  }
}
