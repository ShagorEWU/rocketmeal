import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:get/get.dart';
//import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

import 'package:rocketMeal/Presentation/Profile/profile_screen.dart';
import 'package:rocketMeal/Router/routeName.dart';
import 'package:rocketMeal/Router/routes.dart';

BuildContext testContext;

class BottomNavBar extends StatefulWidget {
  const BottomNavBar({
    Key key,
    @required this.activeBar,
  }) : super(key: key);

  final int activeBar;
  @override
  _BottomNavBarState createState() => _BottomNavBarState();
}

class _BottomNavBarState extends State<BottomNavBar> {
  int _currentIndex;
  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
    _currentIndex == 0
        ? Get.toNamed(RouteName.homeScreen)
        : _currentIndex == 1
            ? Get.toNamed(RouteName.offersScreen)
            : _currentIndex == 2
                ? Get.toNamed(RouteName.favouriteScreen)
                : _currentIndex == 3
                    ? Get.toNamed(RouteName.cartScreen)
                    : Get.toNamed(RouteName.profileScreen);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _currentIndex = widget.activeBar;
  }

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      onTap: onTabTapped,
      currentIndex: _currentIndex,
      selectedItemColor: Colors.green,
      unselectedItemColor: Colors.black,
      items: [
        BottomNavigationBarItem(
          icon: Icon(FlutterIcons.home_fea, semanticLabel: "Home"),
          label: "Home",
        ),
        BottomNavigationBarItem(
            icon: Icon(
              FlutterIcons.ios_menu_ion,
              semanticLabel: "Offers",
            ),
            label: "Offers"),
        BottomNavigationBarItem(
            icon: Icon(FlutterIcons.favorite_border_mdi,
                semanticLabel: "Favourite"),
            label: "Favourite"),
        BottomNavigationBarItem(
            icon: Icon(FlutterIcons.handbag_sli, semanticLabel: "cart"),
            label: "cart"),
        BottomNavigationBarItem(icon: Icon(Icons.people), label: "Profile"),
      ],
    );
  }
}
