import 'dart:io';

import 'package:get/get_rx/get_rx.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class DatabaseHelper {
  static final _dbName = "myDatabase.db";
  static final _dbVersion = 1;
  static final _tableName = "orderInfo";

  static final _name = "name";
  static final _price = "price";
  static final _amount = "amount";

//Singleton class
  DatabaseHelper._privateConstructor();
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();
  static Database _database;
  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await _initiatedatabase();
    return _database;
  }

  //Initiate Database

  _initiatedatabase() async {
    Directory directory = await getApplicationDocumentsDirectory();
    String path = join(directory.path, _dbName);
    return await openDatabase(path, version: _dbVersion, onCreate: _onCreate);
  }

  Future _onCreate(Database db, int version) async {
    db.execute("""
      CREATE TABLE $_tableName(
        $_name TEXT NUT NULL PRIMARY KEY,
        $_price INTEGER ,
        $_amount INTEGER
      )

      """);
  }

  insert(Map<String, dynamic> row) async {
    Database db = await instance.database;

    int sss = await db.insert(
      _tableName,
      row,
    );
  }

  Future<List<Map<String, dynamic>>> queryAll() async {
    Database db = await instance.database;

    return await db.query(_tableName);
  }

  update(Map<String, dynamic> row) async {
    Database db = await instance.database;
    int xx = await db
        .update(_tableName, row, where: '$_name = ?', whereArgs: [row[_name]]);
  }

  delete(String name) async {
    Database db = await instance.database;

    return await db.delete(_tableName, where: '$_name = ?', whereArgs: [name]);
  }

  Future querySingleRow(String name) async {
    Database db = await instance.database;
    // return
    //  Sqflite.firstIntValue(await db
    //     .rawQuery("SELECT COUNT(*) FROM $_tableName WHERE $_name=$name"));

    //return await db.query(_tableName, where: '$_name = ?', whereArgs: [name]);
    List<Map<String, dynamic>> result = await db
        .rawQuery("select $_amount from $_tableName where $_name='$name'");

    print("######## select $_amount from $_tableName where $_name='$name'");

    int value = 0;

    if (result.length == 0) {
      value = 0;
    } else {
      value = result[0]["$_amount"];
    }

    return value;
  }
}
