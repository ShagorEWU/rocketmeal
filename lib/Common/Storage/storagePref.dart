import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class StoragePref {
  GetStorage getStorageOBJ = Get.find();

  storageRead(String key) {
    return getStorageOBJ.read(key);
  }

  storageWrite(String key, dynamic value) {
    getStorageOBJ.write(key, value);
  }

  storageValDelete(String key) {
    getStorageOBJ.remove(key);
  }
}
