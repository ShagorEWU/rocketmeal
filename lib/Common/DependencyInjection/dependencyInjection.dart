import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:rocketMeal/API/Provider/LoginApiProvider.dart';
import 'package:rocketMeal/API/Provider/RegistrationApiProvider.dart';
import 'package:rocketMeal/API/Repository/LoginRepository.dart';
import 'package:rocketMeal/API/Repository/RegistrationRepository.dart';

import 'package:rocketMeal/Common/Methods/CommonMethod.dart';
import 'package:rocketMeal/Common/Methods/Notifications.dart';
import 'package:rocketMeal/Common/Storage/storagePref.dart';
import 'package:rocketMeal/Controller/bottomshow.dart';
import 'package:rocketMeal/Controller/item.dart';
import 'package:rocketMeal/Router/routes.dart';
import 'package:rocketMeal/Presentation/FoodItemShow/Bloc/item_show_bloc.dart';

class DependencyInjection implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<Notifications>(() => Notifications(), fenix: true);
    Get.lazyPut<CommonMethod>(() => CommonMethod(), fenix: true);
    Get.lazyPut<LoginRepository>(() => LoginRepository(), fenix: true);
    Get.lazyPut<LoginApiProvider>(() => LoginApiProvider(), fenix: true);
    Get.lazyPut<RegistrationRepository>(() => RegistrationRepository(),
        fenix: true);
    Get.lazyPut<RegistrationApiProvider>(() => RegistrationApiProvider(),
        fenix: true);
    Get.lazyPut<GetStorage>(() => GetStorage(), fenix: true);
    Get.lazyPut<StoragePref>(() => StoragePref(), fenix: true);
    Get.lazyPut<Routes>(() => Routes(), fenix: true);
    // Get.create<ItemController>(() => ItemController(),permanent: true);
    Get.lazyPut(() => BottomShowController(), fenix: true);

    Get.lazyPut<ItemController>(() => ItemController(), fenix: true);
    Get.lazyPut<ItemShowController>(() => ItemShowController(), fenix: true);
    //BottomShowController icontroller = Get.put(BottomShowController());
  }
}
